final class Vec {

    private final double x;
    private final double y;
    private final double z;

    Vec(final double x, final double y, final double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "( " +
                "x = " + x +
                ", y = " + y +
                ", z = " + z +
                " )";
    }

    public double VecLen() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    public double VecScalar(Vec tmp) {
        return x * tmp.x + y * tmp.y + z * tmp.z;
    }

    public double getX() { return x; }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public Vec VecMulti(Vec tmp) {
        return new Vec(y * tmp.z - z * tmp.y, z * tmp.x - x * tmp.z, x * tmp.y - y * tmp.x);
    }

    public double CosVec(Vec tmp) {
        return VecScalar(tmp) / (VecLen() * tmp.VecLen());
    }

    public Vec Sum(Vec tmp) {
        return new Vec(x + tmp.x, y + tmp.y, z + tmp.z);
    }

    public Vec Diff(Vec tmp) {
        return new Vec(x - tmp.x, y - tmp.y, z - tmp.z);
    }

    public static Vec[] Fill_in_mas_vec(int N) {
        Vec[] array = new Vec[N];
        for (int i = 0; i < N; i++) {
            array[i] = new Vec(Math.random() * 40 - 20, Math.random() * 40 - 20, Math.random() * 40 - 20);
        }
        return array;
    }
}
