import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        Integer[][] array = new Integer[2][2];
        array[0][0] = 1;
        array[0][1] = 2;
        array[1][0] = 3;
        array[1][1] = 4;


        Iterator iter = MatrixIter(array);
        System.out.println(iter.next());
        iter.remove();
        System.out.println(iter.next());
        System.out.println(iter.next());
        System.out.println(iter.next());
        System.out.println(iter.next());
        System.out.println(iter.next());



    }

    public static <T> Iterator<T> MatrixIter (T[][] array) {

        return new Iterator<T>() {
            private int row = 0;
            private int column = 0;

            public boolean hasNext() {
                if (row + 1 == array.length) {
                    return column < array[row].length;
                } else {
                    return row < array.length;
                }
            }

            public T next() {
                if (hasNext()) {
                    if (column == array[row].length) {
                        column = 0;
                        row++;
                    }
                    return array[row][column++];

                }
                else{
                    return (T) "Out of length";
                }
            }

            public void remove(){
                array[row][column]=null;
                next();
            }
        };

    }
}


