import java.util.*;


public class Main {
    public static void main(String[] args) {
        Map<String, String> test=new HashMap<>();
        test.put("1","four");
        test.put("2","one");
        test.put("3","one");
        test.put("4","four");
        test.put("5","four");
        test.put("6","four");
        test.put("7","seven");

        System.out.println(test);
        System.out.println("=================");
        System.out.println(reverse(test));
        System.out.println("=================");
        System.out.println(reverse(reverse(test)));
    }


    public static Map<Object, Collection<Object>> reverse (Map oldMap) {
        Map<Object, Collection<Object>> newMap = new HashMap<>();
        Object[] keys = oldMap.keySet().toArray();
        Object[] values = oldMap.values().toArray();

        for (int i = 0; i < keys.length; i++) {
            ArrayList<Object> temp = new ArrayList<>();
            if (i == keys.length - 1 && newMap.get(values[i]) == null) {
                newMap.put(values[i], Collections.singleton(keys[i]));
            }
            if (newMap.get(values[i]) != null) {
                continue;
            } else {
                for (int j = 0; j < keys.length; j++) {
                    if (values[i].equals(values[j])) {
                        temp.add(keys[j]);
                    }
                }
                newMap.put(values[i], temp);
            }
        }
        return newMap;
    }
}
