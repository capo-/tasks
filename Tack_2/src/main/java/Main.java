import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.FileReader;

import com.opencsv.CSVReader;

public class Main {
    public static void main(String[] args) throws Exception{
        SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat outputDate = new SimpleDateFormat("yyyyMMdd");
        System.out.print("Input a date in format yyyy-MM-dd: ");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String SearchedDate = in.readLine();
        Date date = inputDate.parse(SearchedDate);
        String csvFileName="C:/Users/Vlad/Downloads/FileJ.csv";
        String[] line;
        ArrayList<String> Ogr = new ArrayList<>();


        CSVReader reader = new CSVReader(new FileReader(csvFileName),',','\"',1);

        while((line = reader.readNext()) != null){
            String start = line[10];
            String end = line[11];
                if((date.after(outputDate.parse(start)) && date.before(outputDate.parse(end))) || (date.compareTo(outputDate.parse(start))==0)){
                    if (!Ogr.contains(line[8])) {
                        Ogr.add(line[8]);
                    }
                }
        }
        if(Ogr.isEmpty()){
            System.out.println("Ограничений нет");
        }
        else {
            Ogr.forEach(obj->System.out.println(obj));
        }

    }
}
